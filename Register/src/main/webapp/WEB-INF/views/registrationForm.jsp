<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</head>
<body>
   <h1>User Registration Form</h1>
   <hr />

   <form:form action="saveUser" method="post" modelAttribute="user">
      <table>
         <tr>
            <td>First Name</td>
            <td><form:input path="firstName" id="fName"/></td>
         </tr>
         <tr>
            <td>Last Name</td>
            <td><form:input path="lastName" id="lName"/></td>
         </tr>
         <tr>
            <td>Username *</td>
            <td><form:input path="username" id="uName"/></td>
         </tr>
         <tr>
            <td>Password</td>
            <td><form:password path="password" id="pwd"/></td>
         </tr>
         <tr>
            <td>Email *</td>
            <td><form:input path="email" id="email"/></td>
         </tr>
         <tr>
            <td></td>
            <td><form:button id="submit">Submit</form:button></td>
         </tr>
      </table>
   </form:form>
  
<!--  $("#submit-email").click(function(){
	    var email = $("#email").val();
	    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    if (!filter.test(email)) {
	       alert('The email address you provide is not valid');
	       $("#email").focus();
	       return false;
	    } else {
	       
	    }
	 }); -->
</body>
   <script type="text/javascript">
   $(document).ready(function() {
	   $("#submit").click(function(e) {
		   
	  
	   var name = $("#uName").val();
	   var email = $("#email").val();
	   /* var message = $("#message").val();
	   var contact = $("#contact").val(); */
	   
	   if (name == '' || email == '') {
		   alert("Please Fill Required Fields");
		   e. preventDefault();
		   }
	   if(email!=null){
		   var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		    if (!filter.test(email)) {
		       alert('The email address you provide is not valid');
		       $("#email").focus();
		       e. preventDefault();
	   }}
	   
	   });
   });
   </script>
</html>